#!/usr/bin/env pythoh3

import sys

items = {} #diccionario con cantidades

def op_add():
    """Add an item and quantity from argv to the items dictionary"""
    try:
        item = sys.argv[0]
        quantity = int(sys.argv[1])
    except ValueError:
        sys.exit("All arguments must be integers")

    if items.get(item):
        items[item] = quantity
    if not items.get(item)!=quantity:
        items.setdefault(item, quantity)

def op_items():
    """Print all items, separated by spaces"""
    read = []
    if len(items) != 0:
        for item, quantity in items.items():
            read.append(item)
        stritems =''.join(read)
        print(f"Items: {stritems}")
    if not len(items) != 0:
        print("There is no items added")

def op_all():
    """Print all items and quantities, separated by spaces"""
    read2= []
    if len(items) > 0:
        for item, quantity in items.items():
            read2.append(f"{item} ({quantity})")
        stritems2 =''.join(read2)
        print(f"All: {stritems2}")
    else:
        print("There is no items added")

def op_sum():
    """Print sum of all quantities"""
    suma = 0
    x = items.values()
    for i in x:
        suma = int(i) + suma
        print(suma)

def main():
    while sys.argv:
        op = sys.argv.pop(0)
        if op == "add":
            op_add()
        elif op == "items":
            op_items()
        elif op == "sum":
            op_sum()
        elif op == "all":
            op_all()
if __name__ == '__main__':
    main()
